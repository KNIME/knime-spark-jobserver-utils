package com.knime.bigdata.spark.jobserver.server

import org.apache.spark.api.java.JavaPairRDD
import org.apache.spark.api.java.JavaRDD
import org.apache.spark.api.java.function.Function
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.sql.Row
import org.apache.spark.mllib.linalg.Vectors
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.mllib.regression.LabeledPoint
import scala.collection.mutable.Map

/**
 * converts various intermediate Java RDD forms to JavaRDD of type JavaRDD[Row] or vice versa
 * @author dwk
 */
object RDDUtils {
  val INVALID_LABEL: Int = -1;

  /**
   * merges the JavaPairRDD of Vector and some column to a JavaRDD of Rows
   * @param aPairRdd Row JavaPairRDD to be converted
   */
  def toJavaRDDOfRows[T](aPairRdd: JavaPairRDD[Vector, T]): JavaRDD[Row] = {
    aPairRdd.map(new Function[(Vector, T), Row]() {
      @Override
      def call(tuple: Tuple2[Vector, T]): Row = {
        val v: ArrayBuffer[Any] = new ArrayBuffer(tuple._1.size + 1)
        v.append(tuple._1.toArray: _*)
        v.append(tuple._2)
        Row(v: _*)
      }
    });
  }

  /**
   * merges the JavaPairRDD of Row and some column to a JavaRDD of Rows
   * @param aPairRdd Row JavaPairRDD to be converted
   */
  def addColumn[T](aPairRdd: JavaPairRDD[Row, T]): JavaRDD[Row] = {
    aPairRdd.map(new Function[(Row, T), Row]() {
      @Override
      def call(tuple: Tuple2[Row, T]): Row = {
        val r = tuple._1
        val vals = (0 to r.length - 1).map(ix => {
          r.get(ix) match {
            case v => v
          }
        })
        val v: ArrayBuffer[Any] = new ArrayBuffer(r.length + 1)
        v.insertAll(0, vals)
        v.append(tuple._2)
        Row(v: _*)
      }
    });
  }

  /**
   * convert a RDD of Rows to JavaRDD of Vectors, keeping all indices, the implicit
   * assumption is that all indices are numeric
   * @param aInputRdd Row RDD to be converted
   * @throws IllegalArgumentException if non-numeric values are encountered
   */
  def toJavaRDDOfVectors(aInputRdd: JavaRDD[Row]): JavaRDD[Vector] = {
    aInputRdd.map(new Function[Row, Vector]() {
      @Override
      def call(r: Row): Vector = {
        val vals = (0 to r.length - 1).map(ix => {
          r.get(ix) match {
            case d: Number => d.doubleValue()
            case s => {
              throw new IllegalArgumentException("unsupported row value: " + s)
            }
          }
        }).toArray
        return Vectors.dense(vals);
      }
    })
  }

  /**
   * (Java friendly version)
   * convert a RDD of Rows to JavaRDD of Vectors, keeping only the given indices, the implicit
   * assumption is that all remaining indices are numeric
   * @param aInputRdd Row RDD to be converted
   * @param aNumericColumnIdx array of indices to be kept out
   * @throws IllegalArgumentException if non-numeric values are encountered
   */
  def toJavaRDDOfVectorsOfSelectedIndices(aInputRdd: JavaRDD[Row], aNumericColumnIdx: java.util.List[Integer]): JavaRDD[Vector] = {
    toJavaRDDOfVectorsOfSelectedIndices(aInputRdd, toArray(aNumericColumnIdx))
  }

  private def toArray(list: java.util.List[Integer]): Array[Int] = {
    list.toArray.map(_.asInstanceOf[Int])
  }

  /**
   * convert a RDD of Rows to JavaRDD of Vectors, keeping only the given indices, the implicit
   * assumption is that all remaining indices are numeric
   * @param aInputRdd Row RDD to be converted
   * @param aNumericColumnIdx array of indices to be kept (note that the vector will
   * contain the values in the order as the indices are given)
   * @throws IllegalArgumentException if non-numeric values are encountered
   */
  def toJavaRDDOfVectorsOfSelectedIndices(aInputRdd: JavaRDD[Row], aNumericColumnIdx: Array[Int]): JavaRDD[Vector] = {

    aInputRdd.map(new Function[Row, Vector]() {
      @Override
      def call(r: Row): Vector = {
        Vectors.dense(getDoubleFeatures(r, aNumericColumnIdx))
      }
    })
  }

  /**
   * extract the features from a RDD of labeled points
   * @param aInputRdd Row RDD to be converted
   */
  def toVectorRDDFromLabeledPointRDD(aInputRdd: JavaRDD[LabeledPoint]): JavaRDD[Vector] = {
    aInputRdd.map(new Function[LabeledPoint, Vector]() {
      @Override
      def call(r: LabeledPoint): Vector = {
        r.features
      }
    })
  }

  /**
   * convert a RDD of Rows to JavaRDD of LabeledPoint,
   * all other indices must be numeric
   * @param inputRDD Row RDD to be converted
   * @param labelColumnIndex index of label column (can be numeric or string)
   * @throws IllegalArgumentException if values are encountered that are not numeric
   */
  def toJavaLabeledPointRDD(inputRDD: JavaRDD[Row], labelColumnIndex: Int): JavaRDD[LabeledPoint] = {
    inputRDD.map(new Function[Row, LabeledPoint]() {
      @Override
      def call(row: Row): LabeledPoint = {
        val values = (0 until row.length).map(ix => {
          getDouble(row, ix)
        }).toArray

        val (prefix, suffix) = values.splitAt(labelColumnIndex);

        LabeledPoint(suffix.head, Vectors.dense(prefix ++ suffix.tail));
      }
    })
  }

  def getDouble(row: Row, columnIndex: Int): Double = {
    row.get(columnIndex) match {
      case number: Number => number.doubleValue()
      case null => throw new IllegalArgumentException("Missing values not supported (column index: %s)!" format(columnIndex))
      case other          => throw new IllegalArgumentException("Value of non-numeric type: " + other)
    }
  }

  private def getDoubleFeatures(row: Row, selectedColumns: Array[Int]): Array[Double] = {
    for (selectedColumn <- selectedColumns) yield getDouble(row, selectedColumn)
  }

  def toJavaLabeledPointRDD(inputRDD: JavaRDD[Row], labelColumnIndex: Int, selectedColumns: Array[Int]): JavaRDD[LabeledPoint] = {
    inputRDD.map(new Function[Row, LabeledPoint]() {
      @Override
      def call(row: Row): LabeledPoint = {
        val features = getDoubleFeatures(row, selectedColumns)
        val label = getDouble(row, labelColumnIndex)

        LabeledPoint(label, Vectors.dense(features))
      }
    })
  }

  def toJavaLabeledPointRDD(inputRDD: JavaRDD[Row], labelColumnIndex: Int, selectedColumns: java.util.List[Integer]): JavaRDD[LabeledPoint] = {
    toJavaLabeledPointRDD(inputRDD, labelColumnIndex, toArray(selectedColumns));
  }

  /**
   * puts names of currently referenced named RDDs into java Set
   */
  def activeNamedRDDs(job: KnimeSparkJobWithNamedRDD) = {
    val names: java.util.Set[String] = new java.util.HashSet[String];
    val iter = job.namedRdds.getNames();
    iter.foreach { name => names.add(name) }
    names;
  }

}
