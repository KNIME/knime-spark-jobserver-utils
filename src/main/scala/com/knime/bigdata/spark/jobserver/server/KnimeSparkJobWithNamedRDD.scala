package com.knime.bigdata.spark.jobserver.server

import org.apache.spark.api.java.JavaRDD
import org.apache.spark.api.java.JavaRDD.toRDD
import spark.jobserver.NamedRddSupport
import spark.jobserver.SparkJob
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row
import org.apache.spark.storage.StorageLevel

/**
 * Scala / Java wrapper for the job-servers named rdd support
 * @author dwk
 */
abstract class KnimeSparkJobWithNamedRDD extends NamedRddSupport with SparkJob {

  private val defaultStorageLevel = StorageLevel.NONE;

  /**
   * Stores the given Java RDD of rows under the given key in the map of named rdds
   * (with default storage level and without forcing computation)
   */
  def addJavaRdd(aKey: String, aJavaRdd: JavaRDD[Row]) {
    namedRdds.update(aKey, aJavaRdd, false, defaultStorageLevel)
  }

  /**
   * Stores the given Java RDD of rows under the given key in the map of named rdds
   * with the given storage level (without forcing computation).
   */
  def addJavaRdd(aKey: String,
    aJavaRdd: JavaRDD[Row],
    storageLevel: StorageLevel) {

    namedRdds.update(aKey, aJavaRdd, false, storageLevel)
  }

  /**
   * Stores the given Java RDD of rows under the given key in the map of named rdds,
   * forcing computation as specified (with default storage level).
   */
  def addJavaRdd(aKey: String,
    aJavaRdd: JavaRDD[Row],
    forceComputation: Boolean) {

    namedRdds.update(aKey, aJavaRdd, forceComputation, defaultStorageLevel)
  }

  /**
   * Stores the given Java RDD of rows under the given key in the map of named rdds,
   * with the given storage level and forcing computation as specified.
   */
  def addJavaRdd(aKey: String,
    aJavaRdd: JavaRDD[Row],
    forceComputation: Boolean,
    storageLevel: StorageLevel) {

    namedRdds.update(aKey, aJavaRdd, forceComputation, storageLevel)
  }


  /**
   * Fetches previously stored Java RDD from named rdd map.
   * Note that the caller must ensure that the rdd actually exists in the map before
   * calling this method !
   * @return the named rdd for the given key
   */
  def getJavaRdd(aKey: String): JavaRDD[Row] = {
    namedRdds.get[Row](aKey).get
  }


  /**
   * Validate whether a named object is stored under the given key.

   * @return true if the named object exists, false otherwise.
   */
  def validateNamedObject(aKey: String): Boolean = {
    namedRdds.get(aKey).isDefined
  }


  /**
   * Validates whether a named object is stored under the given key and removes
   * it from the map if so. Calling this method for non-existing named RDDs has no effect
   */
  def deleteNamedObject(aKey: String) {
    if (validateNamedObject(aKey)) {
      namedRdds.destroy(aKey)
    }
  }

  /**
   * @return Ids of currently referenced named named objects (both RDDs and JavaRDDs) as a Java Set
   */
  def getNamedObjects(): java.util.Set[String] = {
    val names: java.util.Set[String] = new java.util.HashSet[String];
    val iter = namedRdds.getNames();
    iter.foreach { name => names.add(name) }
    names;
  }

  /**
   * Stores the given Java RDD of rows under the given key in the map of named rdds
   * (with default storage level and without forcing computation)
   */
  def addRdd[T](aKey: String, aRdd: RDD[T]) {
    namedRdds.update(aKey, aRdd, false, defaultStorageLevel)
  }

  /**
   * Stores the given Java RDD of rows under the given key in the map of named rdds
   * with the given storage level (without forcing computation).
   */
  def addRdd[T](aKey: String,
    aRdd: RDD[T],
    storageLevel: StorageLevel) {

    namedRdds.update(aKey, aRdd, false, storageLevel)
  }

  /**
   * Stores the given Java RDD of rows under the given key in the map of named rdds,
   * forcing computation as specified (with default storage level).
   */
  def addRdd[T](aKey: String,
    aRdd: RDD[T],
    forceComputation: Boolean) {

    namedRdds.update(aKey, aRdd, forceComputation, defaultStorageLevel)
  }

  /**
   * Stores the given Java RDD of rows under the given key in the map of named rdds,
   * with the given storage level and forcing computation as specified.
   */
  def addRdd[T](aKey: String,
    aRdd: RDD[T],
    forceComputation: Boolean,
    storageLevel: StorageLevel) {

    namedRdds.update(aKey, aRdd, forceComputation, storageLevel)
  }

  /**
   * fetches previously stored rdd from named rdd map
   * note that the caller must ensure that the rdd actually exists in the map before
   * calling this method !
   * @return the named rdd for the given key
   */
  def getRdd[T](aKey: String): RDD[T] = {
    namedRdds.get[T](aKey).get
  }
}
