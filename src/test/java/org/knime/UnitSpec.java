package org.knime;

import org.junit.BeforeClass;


/**
 * 
 * @author dwk
 *
 */
public abstract class UnitSpec {
	
	@BeforeClass
	public static void beforeSuite() {
		//TODO - we need a dummy RestClient to be able to test anything locally
		//KnimeConfigContainer.config = KnimeConfigContainer.config.withValue("spark.jobServer", ConfigValueFactory.fromAnyRef("localhost"));
		//KnimeConfigContainer.config = KnimeConfigContainer.config.withValue("spark.jobServerPort", ConfigValueFactory.fromAnyRef("8090"));		
	}
	
}