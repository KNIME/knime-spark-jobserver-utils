#  KNIME Job Server Utilities

NOTE: This branch is for Spark 1.2.1 and Spark jobserver 0.7.0-SNAPSHOT

The KNIME Job Server Utilities integrate the KNIME Spark Executor extension with
the Spark job server, that provides a REST-based interface to start, stop and
manage Spark jobs.

# Building

NOTE: To build this project as described below, you need Linux. On Windows more
fiddling is required to build the Spark jobserver in the correct version.

## Build the Spark Job Server provided by KNIME

The Spark Jobserver provided by KNIME, backports a (more or less) up-to-date
version of the official Spark Jobserver to an older Spark version. The easiest 
way to build it, is to checkout and run KNIME's buildscript:

```
$ git clone git@bitbucket.org:teamknime/spark-jobserver-buildscripts.git
$ cd spark-jobserver-buildscripts
$ ./build-jobservers.sh
```   

Aside  from building the jobserver in several editions (Cloudera, etc), it also
publishes the jobserver-api into the local maven cache. This is required to build
the KNIME Job Server Utilities as follows:

```
$ git clone git@bitbucket.org:colognescala/spjs.git
$ cd spjs
$ git checkout <THISBRANCH>
$ mvn package
```

Now, under target/knime-jobserver-utils-<VERSION>.jar you can find the jar to
be included in the KNIME extension.
